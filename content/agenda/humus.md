Title: Comment réduire son empreinte numérique avec le logiciel libre, tout en préservant sa vie privée ?
Date: 2022-05-07 10:00
Modified: 2022-05-07 10:00
Tags: logiciel libre, écologie, environnement, empreinte écologique, vie privée
Slug: humus
Lang: fr
Authors: libreallier
Summary: Conférence sur le logiciel en tant que solution au défi écologique.

Après avoir présenté l'empreinte écologique du numérique et les enjeux liés,
nous verrons en quoi le logiciel libre fait partie de la solution en
luttant notamment contre l'obsolescence programmée et en limitant également
le volume des métadonnées échangées. Ce qui contribue au respect de la vie
privée.

Vous pouvez retrouvez ici le [support] de la conférence.

[support]: ../doc/humus_logiciel-libre_ecologie.pdf
