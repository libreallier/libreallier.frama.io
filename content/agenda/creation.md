Title: Création du mouvement LibreAllier
Date: 2021-02-15 15:00
Modified: 2021-02-15 15:00
Tags: libre, allier
Slug: creation
Lang: fr
Authors: libreallier
Summary: Naissance du mouvement pour le logiciel libre dans l'Allier

Les principes fondamentaux sont ainsi définis :

* Nous œuvrons à multiplier l'usage et l'adoption du logiciel libre
* Au-delà de l'entre-aide, nous souhaitons fédérer les initiatives autour du logiciel libre dans l'Allier
* Le mouvement se structure autour d'un [service] de messagerie instantanée libre

Ensemble, nous formons les alliés du logiciel libre dans le 03 (Auvergne, France). 

[service]: https://framateam.org/libreallier
