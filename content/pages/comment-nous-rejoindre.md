Title: Comment nous rejoindre ?
Slug: comment-nous-rejoindre
Authors: libreallier
Summary: Structuration des alliés du logiciel libre dans le 03

Le mouvement pour le logiciel libre dans l'Allier se structure autour d'une
[communauté Framateam][service].
Ce service de messagerie instantanée est propulsé par l'instance **Mattermost**
de Framasoft, qui repose sur un logiciel libre publié sous licence MIT.

Pour nous rejoindre, il suffit de créer un compte sur la plate-forme Framateam.
Vous serez alors redirigé sur l'espace de l'équipe. Il existe plusieurs
canaux sur cet espace, nous vous invitons à utiliser le canal général
(par défaut) pour vous présenter.

[service]: https://framateam.org/libreallier
