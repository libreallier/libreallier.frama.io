Title: Logiciel libre
Slug: logiciel-libre
Authors: libreallier
Summary: Présentation du logiciel libre

Un logiciel est libre s'il respecte les libertés numériques des
utilisateurs, s'il ne les place pas sous la domination d'un éditeur de logiciels,
qui se trouve généralement être une multinationale des états-unis qui pratique l'optimisation fiscale en Europe.

### Bien commun
Le logiciel libre permet de concevoir l'informatique comme un bien commun
respectueux de la vie privée et des libertés.
Les ordinateurs équipés d'un système d'exploitation GNU/Linux et de différentes applications libres,
permettent de naviguer sur Internet, lire et écrire des courriels, créer des documents,
des présentations et des feuilles de calculs, gérer une bibliothèque multimédia et bien plus encore.

Notre action ne serait pas possible sans le travail en amont de nombreux
informaticiens qui ont développé, préparé ces logiciels et qui nous permettent
de les utiliser sans nécessairement avoir à payer de redevances à un quelconque éditeur.
Le logiciel libre n'est **pas gratuit**, il repose sur des heures de travail,
sur la formation de nombreux informaticiens, qui ont ensuite été rémunérés
pour participer à des projets qui contribuent à ce bien commun.

### Réutilisation
Le logiciel libre peut fonctionner sur des ordinateurs très récents, mais
aussi très anciens. Pour la même fonctionnalité, les utilisateurs ont le
choix d'utiliser différents logiciels en fonction des capacités de leur
ordinateur ou de leurs envies. Ainsi, ils ne subissent pas le choix
d'un éditeur qui impose une interface unique.

Ce n'est pas parce que vous mettez à jour votre ordinateur que
vos périphériques vont cesser de fonctionner. Les mises à jour de systèmes
libres contribuent à l'amélioration générale de la sécurité, pas à vous
imposer des choix.

Le logiciel libre est donc un moyen de prolonger la vie des ordinateurs,
de lutter contre l'obsolescence programmée, non pas par les constructeurs
d'ordinateur, mais par des éditeurs de logiciels comme Microsoft.

> Les systèmes Windows XP et W7 ne sont maintenant plus mis à jour par
leur éditeur Microsoft. Si vous continuez de les utiliser, vous mettez en
danger la sécurité de vos données personnelles, ainsi que celle de vos
ami-e-s. Nous vous invitons à installer au plus vite une distribution
GNU/Linux et ainsi à libérer vos ordinateurs tout en contribuant à
l'amélioration générale de la sécurité (un ordinateur non mis à jour
peut être le point de départ de nombreuses cyberattaques).

### Économie
Le logiciel libre contribue au développement de l'économie locale. Un
logiciel développé à Paris ou à Londres peut être repris, personnalisé
pour un autre besoin à Vichy ou Clermont-Ferrand.
L'ensemble de ces développements contribue à l'amélioration générale
du projet.

Le logiciel libre permet à n'importe quel acteur économique de proposer un service,
sans voir à développer la moindre ligne de code. Bien souvent, cet acteur
économique devra bien sûr travailler en amont, notamment à la sécurité du service proposé.
Mais la mise en place de ce service ne suppose pas un développement préalable
de millions de lignes de code et c'est en cela que le logiciel libre
contribue au développement de l'économie locale : il en démocratise les usages...

Si notre acteur économique a la chance d'obtenir des commandes d'autres
partenaires locaux (institutions, entreprises), il contribuera en retour
au financement de l'économie en s'acquittant de sa part sociale. Avec
les années, son expérience sur le logiciel utilisé lui permettra aussi
peut-être de contribuer en retour à son développement. S'il n'a pas encore
acquis suffisamment d'expérience, il pourra toujours effectuer un don
à la communauté des développeurs.
