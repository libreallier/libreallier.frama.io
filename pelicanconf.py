#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Libreallier'
TITLE = 'Libre Allier'
SITENAME = '< Libre@llier >'
SITEURL = ''

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'Europe/Paris'
DEFAULT_DATE_FORMAT = '%d %B %Y'
DEFAULT_LANG = 'fr'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
"""
LINKS = (('April', 'https://www.april.org/'),
         ('Linux Arverne', 'https://www.linuxarverne.org/'),)
"""
# Social widget
"""
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)
"""

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# personnalisation
THEME = 'themes/html5-dopetrope'
STATIC_PATHS = ( "doc/", )

HEADER_TITLE = 'Mouvement pour le logiciel libre'
HEADER_SUBTITLE = 'dans l\'Allier'
STAR_HEADER = 'Libre'
STAR_TEXT = 'Un logiciel est libre s\'il respecte les libertés numériques des utilisateurs, s\'il ne les place pas sous la domination d\'un éditeur de logiciels.'
SOLIDAR_HEADER = 'Communauté'
SOLIDAR_TXT = 'Ce mouvement pour le logiciel libre dans l\'Allier se structure autour d\'un <a href="https://framateam.org/libreallier" target="_blank">service</a> de messagerie instantanée libre.'
ORGA_HEADER = 'Allier'
ORGA_TEXT = 'Au-delà de l\'entre-aide, nous souhaitons fédérer les initiatives autour du logiciel libre dans l\'Allier et en favoriser l\'usage.'
LIBRE_LINK = 'pages/logiciel-libre.html'
LIBRE_LAB = 'Découvrir le logiciel libre'
SOLIDAIRE_LINK = 'pages/comment-nous-rejoindre.html'
SOLIDAIRE_LAB = 'Comment nous rejoindre'
ABOUT_IMAGE = 'theme/images/logo.png'
ABOUT_TEXT = 'Le mouvement pour le logiciel libre dans l\'Allier'
COPYRIGHT = 'LibreAllier. Sauf mention contraire, le contenu est disponible sous licence <a href="https://creativecommons.org/licenses/by-sa/3.0/fr/" target="_blank">CC-BY-SA version 3.0 ou ultérieure</a>'
CODE = '<i class="icon brands fa-gitlab" aria-hidden="true"></i> Hébergé sur <a href="https://framagit.org/libreallier/libreallier.frama.io" target="_blank">Framagit</a>'
LOCALE = ('fr_FR.utf8', )
#MODAL_TEXT = """\
#Nous organisons le 17/10 de 15 h à 17 h, à l’Atrium de Vichy,
#un atelier d’installation du système libre GNU/Linux, afin de libérer vos
#machines de l’emprise des multinationales américaines.
#"""
#MODAL_LINK = 'install-party-17.html'
#MODAL_LAB = 'Inscription'
